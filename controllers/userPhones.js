const repositories = require('../repositories');

module.exports = async (req, res) => {
	try {
		const telephones = await repositories.telephones.ofUserIdentity(
			req.params.id
		);

		if (!telephones || telephones.length === 0) {
			res.status(404).json({
				status: 'sorry, no telephones found',
				data: { telephones },
			});
		}

		res.status(200).json({
			status: 'success',
			data: { telephones },
		});
	} catch (err) {
		console.log(err);
	}
};
