const jwt = require('jsonwebtoken');
const repositories = require('../repositories');

module.exports = async (req, res) => {
	const { email, password } = req.body;
	const user = await repositories.users.withValidAuth(email, password);

	if (!user) {
		res.status(401).json({
			status: 'fail',
			message: 'sorry, invalid user',
			data: { user: null },
		});

		return;
	}

	const token = jwt.sign(
		{ id: user.id, email: user.email },
		process.env.JWT_SECRET
	);

	res.status(200).json({
		status: 'success',
		data: { token, user: user },
	});
};
