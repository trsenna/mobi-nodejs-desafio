const uuid = require('uuid');
const repositories = require('../repositories');

module.exports = async (req, res) => {
	const identity = uuid.v4();
	await repositories.users.add(identity, req.body);

	const user = await repositories.users.ofIdentity(identity);
	req.body.telephones.forEach((telephone) => {
		const phoneIdentity = uuid.v4();
		repositories.telephones.add(phoneIdentity, telephone, user);
	});

	res.status(201).json({
		status: 'success',
		data: {
			user: {
				id: user.id,
				created_at: user.created_at,
				modified_at: user.modified_at,
			},
		},
	});
};
