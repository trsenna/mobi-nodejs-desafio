const repositories = require('../repositories');

module.exports = async (req, res) => {
	try {
		const users = await repositories.users.search();

		if (!users || users.length === 0) {
			res.status(404).json({
				status: 'sorry, no users found',
				data: { users },
			});
		}

		res.status(200).json({
			status: 'success',
			data: { users },
		});
	} catch (err) {
		console.log(err);
	}
};
