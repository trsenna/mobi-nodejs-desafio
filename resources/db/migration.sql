DROP TABLE IF EXISTS app_users;
DROP TABLE IF EXISTS app_telephones;

CREATE TABLE app_users (
    id              varchar(36),
    name            varchar(50) NOT NULL,
    email           varchar(50) NOT NULL,
    password        varchar NOT NULL,
    created_at      timestamp NOT NULL,
    modified_at     timestamp NOT NULL,
    PRIMARY KEY(id)
);

INSERT INTO app_users VALUES ('b85231d7-7818-434f-ac80-af43bb80cc23', 'Fulano', 'fulano@yopmail.com', 'fulano@secret', NOW(), NOW());
INSERT INTO app_users VALUES ('61967ff3-2eb4-4dc9-85a2-8f0f94dfcb13', 'Ciclano', 'ciclano@yopmail.com', 'ciclano@secret', NOW(), NOW());

CREATE TABLE app_telephones (
    id          varchar(36),
    user_id     varchar(36) NOT NULL,
    number      char(10) NOT NULL,
    area_code   char(3) NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_user
        FOREIGN KEY(user_id) 
	    REFERENCES app_users(id)
);

INSERT INTO app_telephones VALUES ('5f64684f-a5cd-4c37-af7f-8bf1bcc0f7ff', 'b85231d7-7818-434f-ac80-af43bb80cc23', '98710-7765', '011');
INSERT INTO app_telephones VALUES ('53277dd6-3730-4352-89bb-aaeaa69bd490', '61967ff3-2eb4-4dc9-85a2-8f0f94dfcb13', '98710-7722', '011');
INSERT INTO app_telephones VALUES ('d7c4ebbb-2383-424c-8fb1-383a2a17ae4e', '61967ff3-2eb4-4dc9-85a2-8f0f94dfcb13', '98710-7744', '011');
