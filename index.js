const dotenv = require('dotenv');
const app = require('./app');

dotenv.config();
const appPort = process.env.APP_PORT;

if (process.env.NODE_ENV === 'production') {
	app.listen(3000, '0.0.0.0', () => {
		console.log(`app listening at http://0.0.0.0:${appPort}`);
	});
} else {
	app.listen(appPort, () => {
		console.log(`app listening at http://localhost:${appPort}`);
	});
}
