const { Pool } = require('pg');

const pool = new Pool();

module.exports.users = {
	async search() {
		const users = await pool.query(
			'select id, name, email, created_at, modified_at from app_users'
		);

		return users.rows;
	},
	async ofIdentity(identity) {
		const users = await pool.query({
			text: 'select id, name, email, created_at, modified_at from app_users where id = $1',
			values: [identity],
		});

		return users.rows.length !== 0 ? users.rows[0] : null;
	},
	async add(identity, { name, email, password }) {
		await pool.query({
			text: 'insert into app_users values ($1, $2, $3, $4, now(), now())',
			values: [identity, name, email, password],
		});
	},
	async withValidAuth(email, password) {
		const users = await pool.query({
			text: 'select id, name, email from app_users where email = $1 and password = $2',
			values: [email, password],
		});

		return users.rows.length !== 0 ? users.rows[0] : null;
	},
};

module.exports.telephones = {
	async ofUserIdentity(userIdentity) {
		const telephones = await pool.query({
			text: 'select number, area_code from app_telephones where user_id = $1',
			values: [userIdentity],
		});

		return telephones.rows;
	},
	async add(identity, { number, area_code }, user) {
		await pool.query({
			text: 'insert into app_telephones values ($1, $2, $3, $4)',
			values: [identity, user.id, number, area_code],
		});
	},
};
