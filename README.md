# Desafio Node.js

Projeto desenvolvido para atender os requisitos de um processo seletivo. Trata-se de uma API Rest desenvolvida utilizando Node.js, Express.js e JWT. Para persisência dos dados foi utilizado PostgreSQL.

Os endpoints solicitados podem ser acessados em:

- Listar Usuários | GET | http://147.182.255.154:3000/api/v1/user
- Listar Telefones | GET | http://147.182.255.154:3000/api/v1/user/:id/phones
- Sign Up | POST | http://147.182.255.154:3000/api/v1/user
- Sign In | POST | 147.182.255.154:3000/api/v1/user/signin

Escolhi executar o ambiente na [DigitalOcean - The Developer Cloud](https://www.digitalocean.com/)

Apesar do projeto estar liberado para avaliação claramente ainda precisa de melhorias e correções para atender 100% do desafio. Os recursos que fiquei devendo são:

- Criação de testes de unidade
- Validar dados de entrada

Provavelmente existem outros pontos para corrigir, melhorar e refatorar - mas vou limitar o projeto ao que foi possível implementar desde o momento que iniciei sem estourar o prazo de entrega.

## Endpoint Sign Up

POST - http://147.182.255.154:3000/api/v1/user

```json
{
	"name": "Fulano",
	"email": "fulano@yopmail.com",
	"password": "fulano@secret",
	"telephones": [
		{
			"number": "86555-8877",
			"area_code": "011"
		},
		{
			"number": "86955-9977",
			"area_code": "012"
		}
	]
}
```

Retorno

```json
{
	"status": "success",
	"data": {
		"user": {
			"id": "70a093de-4c97-4ca7-8673-81fa02cd838f",
			"created_at": "2021-08-21T22:13:56.635Z",
			"modified_at": "2021-08-21T22:13:56.635Z"
		}
	}
}
```

## Endpoint Sign In

POST - 147.182.255.154:3000/api/v1/user/signin

```json
{
	"email": "fulano@yopmail.com",
	"password": "fulano@secret"
}
```

Retorno

```json
{
	"status": "success",
	"data": {
		"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjcwYTA5M2RlLTRjOTctNGNhNy04NjczLTgxZmEwMmNkODM4ZiIsImVtYWlsIjoiZnVsYW5vQHlvcG1haWwuY29tIiwiaWF0IjoxNjI5NTg1ODcyfQ.n6nxIZ4dFZl8X4keAcMWfxIP7GVYMiqTbruFtElWPy8",
		"user": {
			"id": "70a093de-4c97-4ca7-8673-81fa02cd838f",
			"name": "Fulano",
			"email": "fulano@yopmail.com"
		}
	}
}
```

## Endpoint Listar Usuários

GET | http://147.182.255.154:3000/api/v1/user

Retorno

```json
{
	"status": "success",
	"data": {
		"users": [
			{
				"id": "70a093de-4c97-4ca7-8673-81fa02cd838f",
				"name": "Fulano",
				"email": "fulano@yopmail.com",
				"created_at": "2021-08-21T22:13:56.635Z",
				"modified_at": "2021-08-21T22:13:56.635Z"
			}
		]
	}
}
```

## Endpoint Listar Telefones

GET | http://147.182.255.154:3000/api/v1/user/:id/phones

Retorno

```json
{
	"status": "success",
	"data": {
		"telephones": [
			{
				"number": "86555-8877",
				"area_code": "011"
			},
			{
				"number": "86955-9977",
				"area_code": "012"
			}
		]
	}
}
```
