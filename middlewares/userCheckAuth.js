const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {
	const authHeader = req.headers['authorization'];
	const token = authHeader && authHeader.split(' ')[1];

	if (token === null) {
		return res.status(401).json({
			status: 'fail',
			message: 'no authorization token provided',
		});
	}

	try {
		console.log(token, process.env.JWT_SECRET);
		const user = await jwt.verify(token, process.env.JWT_SECRET);
		console.log(user);
	} catch (err) {
		res.status(403).json({
			status: 'fail',
			message: 'sorry, some authorization error happend',
		});
		console.log(err);
		return;
	}

	next();
};
