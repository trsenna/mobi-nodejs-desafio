const bcrypt = require('bcrypt');

module.exports = async (req, res, next) => {
	if (!req.body.password || req.body.password.length === 0) {
		throw 'sorry, but having a password is required';
	}

	const salt = process.env.BCRYPT_SALT;
	const pass = bcrypt.hashSync(req.body.password, salt);
	req.body.password = pass;

	next();
};
