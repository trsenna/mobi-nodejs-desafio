const express = require('express');
const userPhones = require('../controllers/userPhones');
const userSearch = require('../controllers/userSearch');
const userSignIn = require('../controllers/userSignIn');
const userSignUp = require('../controllers/userSignUp');
const passwordToHash = require('../middlewares/passwordToHash');
const userCheckAuth = require('../middlewares/userCheckAuth');

const router = express.Router();

router.route('/').get(userCheckAuth, userSearch);
router.route('/:id/phones').get(userCheckAuth, userPhones);
router.route('/signin').post(passwordToHash, userSignIn);
router.route('/').post(passwordToHash, userSignUp);

module.exports = router;
